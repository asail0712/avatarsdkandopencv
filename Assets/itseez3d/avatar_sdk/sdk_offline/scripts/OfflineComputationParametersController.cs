﻿/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
* You may not use this file except in compliance with an authorized license
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
* CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
* See the License for the specific language governing permissions and limitations under the License.
* Written by Itseez3D, Inc. <support@avatarsdk.com>, April 2017
*/

using ItSeez3D.AvatarSdk.Core;
using SimpleJSON;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ItSeez3D.AvatarSdk.Offline
{

	/// <summary>
	/// Resource manager for Offline SDK
	/// </summary>
	public class OfflineComputationParametersController : ComputationParametersController
	{
		#region Dll interface
		[DllImport(DllHelper.dll)]
		private unsafe static extern int getParametersJson(AvatarSdkPipelineType pipelineType, byte* parametersJson, int parametersBufferSize);

		[DllImport(DllHelper.dll)]
		private unsafe static extern int getDefaultParametersJson(AvatarSdkPipelineType pipelineType, byte* parametersJson, int parametersBufferSize);

		[DllImport(DllHelper.dll)]
		private unsafe static extern int getCustomParametersJson(AvatarSdkPipelineType pipelineType, byte* parametersJson, int parametersBufferSize);

		[DllImport(DllHelper.dll)]
		private unsafe static extern int getDefaultParametersJsonForStyledFace(byte* parametersJson, int parametersBufferSize);

		[DllImport(DllHelper.dll)]
		private unsafe static extern int getAllParametersJsonForStyledFace(byte* parametersJson, int parametersBufferSize);

		#endregion

		protected const string COMPUTE_LOD_KEY = "compute_LOD";

		/// <summary>
		/// There are three sets of the offline parameters
		/// </summary>
		private enum OfflineParametersSubset
		{
			// parameters available for all users
			Base, 
			// default parameters
			Default,
			// individual parameters
			Custom
		}
		
		private Dictionary<string, ComputationParameters> computationParametersDictionary = new Dictionary<string, ComputationParameters>();
		
		
		/// <summary>
		/// Returns lists of parameters asynchronous
		/// </summary>
		public AsyncRequest<ComputationParameters> GetParametersAsync(ComputationParametersSubset parametersSubset, PipelineType pipelineType)
		{
			var request = new AsyncRequestThreaded<ComputationParameters>(() => { return GetParameters(parametersSubset, pipelineType); }, AvatarSdkMgr.Str(Strings.GettingParametersList));
			AvatarSdkMgr.SpawnCoroutine(request.Await());
			return request;
		}

		/// <summary>
		/// Converts ComputationParameters to the json format for offline computations
		/// </summary>
		public override string GetCalculationParametersJson(PipelineType pipelineType, ComputationParameters computationParams)
		{
			JSONObject resourcesJson = new JSONObject();

			resourcesJson[PIPELINE_KEY] = pipelineType.GetPipelineTypeName();
			resourcesJson[PIPELINE_SUBTYPE_KEY] = pipelineType.GetPipelineSubtypeName();

			if (computationParams != null)
			{
				if (!IsListNullOrEmpty(computationParams.blendshapes.Values))
					resourcesJson[BLENDSHAPES_KEY] = computationParams.blendshapes.ToJson();

				if (!IsListNullOrEmpty(computationParams.haircuts.Values))
					resourcesJson[HAIRCUTS_KEY] = computationParams.haircuts.ToJson();

				if (!IsListNullOrEmpty(computationParams.additionalTextures.Values))
					resourcesJson[ADDITIONAL_TEXTURES] = computationParams.additionalTextures.ToJson(false);

				if (!computationParams.modelInfo.IsEmpty())
					resourcesJson[MODEL_INFO] = computationParams.modelInfo.ToJson(false);

				if (!computationParams.avatarModifications.IsEmpty())
					resourcesJson[AVATAR_MODIFICATIONS] = computationParams.avatarModifications.ToJson(false);

				if (!computationParams.shapeModifications.IsEmpty())
					resourcesJson[SHAPE_MODIFICATIONS] = computationParams.shapeModifications.ToJson();
			}

			if (pipelineType == PipelineType.HEAD)
				resourcesJson[COMPUTE_LOD_KEY] = true;

			return resourcesJson.ToString(4);
		}

		/// <summary>
		/// Returns lists of parameters for a given subset that can be generated for avatar
		/// </summary>
		public ComputationParameters GetParameters(ComputationParametersSubset parametersSubset, PipelineType pipelineType)
		{
			if (computationParametersDictionary.ContainsKey(pipelineType.ParametersSubsetHash(parametersSubset)))
			{
				return computationParametersDictionary[pipelineType.ParametersSubsetHash(parametersSubset)];
			}
			else
			{
				ComputationParameters computationParameters = ComputationParameters.Empty;
				if (parametersSubset == ComputationParametersSubset.DEFAULT)
					computationParameters = GetParametersForOfflineSubset(OfflineParametersSubset.Default, pipelineType);
				else
				{
					// Merge Base parameters with Custom to get the list of all available parameters
					computationParameters = GetParametersForOfflineSubset(OfflineParametersSubset.Base, pipelineType);
					ComputationParameters customParameters = GetParametersForOfflineSubset(OfflineParametersSubset.Custom, pipelineType);
					computationParameters.Merge(customParameters);
				}

				computationParametersDictionary.Add(pipelineType.ParametersSubsetHash(parametersSubset), computationParameters);
				return computationParameters;
			}
		}

		private unsafe ComputationParameters GetParametersForOfflineSubset(OfflineParametersSubset offlineSubset, PipelineType pipelineType)
		{
			byte[] buffer = new byte[32768];
			fixed (byte* rawBytes = &buffer[0])
			{
				if (pipelineType == PipelineType.STYLED_FACE)
				{
					switch (offlineSubset)
					{
						case OfflineParametersSubset.Base:
							getAllParametersJsonForStyledFace(rawBytes, buffer.Length);
							break;
						case OfflineParametersSubset.Default:
							getDefaultParametersJsonForStyledFace(rawBytes, buffer.Length);
							break;
						case OfflineParametersSubset.Custom:
							return ComputationParameters.Empty;
					}
				}
				else
				{
					AvatarSdkPipelineType sdkPipelineType = pipelineType.ToSdkPipelineType();
					switch (offlineSubset)
					{
						case OfflineParametersSubset.Base:
							getParametersJson(sdkPipelineType, rawBytes, buffer.Length);
							break;

						case OfflineParametersSubset.Default:
							getDefaultParametersJson(sdkPipelineType, rawBytes, buffer.Length);
							break;

						case OfflineParametersSubset.Custom:
							getCustomParametersJson(sdkPipelineType, rawBytes, buffer.Length);
							break;
					}
				}
			}

			return GetParametersFromJson(System.Text.Encoding.ASCII.GetString(buffer));
		}
	}
}
