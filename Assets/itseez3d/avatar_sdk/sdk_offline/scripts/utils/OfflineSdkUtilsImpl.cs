﻿/* Copyright (C) Itseez3D, Inc. - All Rights Reserved
* You may not use this file except in compliance with an authorized license
* Unauthorized copying of this file, via any medium is strictly prohibited
* Proprietary and confidential
* UNLESS REQUIRED BY APPLICABLE LAW OR AGREED BY ITSEEZ3D, INC. IN WRITING, SOFTWARE DISTRIBUTED UNDER THE LICENSE IS DISTRIBUTED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OR
* CONDITIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED
* See the License for the specific language governing permissions and limitations under the License.
* Written by Itseez3D, Inc. <support@avatarsdk.com>, April 2017
*/

using ItSeez3D.AvatarSdk.Core;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ItSeez3D.AvatarSdk.Offline
{
	public class OfflineSdkUtilsImpl : OfflineSdkUtils
	{
		public new IEnumerator EnsureSdkResourcesUnpacked(string unpackedResourcesPath)
		{
			var resourceListFilename = "resource_list.txt";

#if UNITY_EDITOR
			// getting list of offline sdk resources and saving it to file
			AssetDatabase.Refresh();

			string resourcesDir = PluginStructure.GetPluginDirectoryPath(PluginStructure.OFFLINE_RESOURCES_DIR, PathOriginOptions.FullPath);
			string miscResourcesDir = PluginStructure.GetPluginDirectoryPath(PluginStructure.MISC_OFFLINE_RESOURCES_DIR, PathOriginOptions.FullPath);
			var offlineResourcesList = new List<string> { resourcesDir, miscResourcesDir };
			var resourceFiles = new List<string>();
			foreach (var resDir in offlineResourcesList)
			{
				PluginStructure.CreatePluginDirectory(resDir);
				GetFileNamesInDirectory(ref resourceFiles, resDir, "", true, resourceExtension);
			}

			File.WriteAllLines(Path.Combine(miscResourcesDir, resourceListFilename), resourceFiles.ToArray());
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
#endif

			var loadingStartTime = Time.realtimeSinceStartup;

			// read list of all resources files
			UnityEngine.Object resourceListObject = null;
			var resourceListPath = GetResourcePathInAssets(resourceListFilename);
			if (Utils.IsDesignTime())
				resourceListObject = Resources.Load(resourceListPath);
			else
			{
				var resourceListRequest = Resources.LoadAsync(resourceListPath);
				yield return resourceListRequest;
				resourceListObject = resourceListRequest.asset;
			}

			var resourceListAsset = resourceListObject as TextAsset;
			if (resourceListAsset == null)
			{
				Debug.LogErrorFormat("Could not read the list of resources from {0}", resourceListFilename);
				yield break;
			}

			// names of all the resources that need to be unpacked
			var resourceList = resourceListAsset.text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

			// verify the integrity of the unpacked resource folder
			bool shouldUnpackResources = false;
			var testFile = Utils.CombinePaths(unpackedResourcesPath, SdkVersion.Version.ToString() + "_copied.test");

			// first of all, check if the indicator file is present in the directory
			if (!File.Exists(testFile))
				shouldUnpackResources = true;

			// get actual list of the unpacked resources
			var unpackedResources = new HashSet<string>();
			foreach (var unpackedResourcePath in Directory.GetFiles(unpackedResourcesPath, "*.*", SearchOption.AllDirectories))
			{
				var unpackedResource = Path.GetFileName(unpackedResourcePath);
				unpackedResources.Add(unpackedResource);
			}

			bool unpackedAllResources = resourceList.All(resource => unpackedResources.Contains(Utils.GetFileName(resource)));
			if (!unpackedAllResources)
				shouldUnpackResources = true;

			if (shouldUnpackResources)
			{
				Debug.LogFormat("Should unpack all resources");
				yield return UnpackResources(resourceList, unpackedResourcesPath, testFile);
				Debug.LogFormat("Took {0} seconds to load resources", Time.realtimeSinceStartup - loadingStartTime);
			}
		}

		public new IEnumerator EnsureInitialized(string unpackedResourcesPath, bool showError = false, bool resetResources = false)
		{
			if (resetResources)
			{
				RemoveExistingUnpackedResources(unpackedResourcesPath);
#if UNITY_EDITOR
				string miscResourcesDir = PluginStructure.GetPluginDirectoryPath(PluginStructure.MISC_OFFLINE_RESOURCES_DIR, PathOriginOptions.RelativeToAssetsFolder);
				AssetDatabase.DeleteAsset(miscResourcesDir);
				PluginStructure.CreatePluginDirectory(miscResourcesDir);
				AssetDatabase.Refresh();
#endif
			}

			string clientId = null, clientSecret = null;
			var accessCredentials = AuthUtils.LoadCredentials();
			if (accessCredentials != null)
			{
				clientId = accessCredentials.clientId;
				clientSecret = accessCredentials.clientSecret;
			}

			var offlineSdkInitializer = new OfflineSdkInitializer();
			string miscResourcesPath = PluginStructure.GetPluginDirectoryPath(PluginStructure.MISC_OFFLINE_RESOURCES_DIR, PathOriginOptions.FullPath);
			yield return offlineSdkInitializer.Run(miscResourcesPath, SdkVersion.Version.ToString(), NetworkUtils.rootUrl, clientId, clientSecret);
			if (!offlineSdkInitializer.Success && showError)
				Utils.DisplayWarning("Could not initialize offline SDK!", "Error message: \n" + offlineSdkInitializer.LastError);

			yield return EnsureSdkResourcesUnpacked(unpackedResourcesPath);
		}
	}
}